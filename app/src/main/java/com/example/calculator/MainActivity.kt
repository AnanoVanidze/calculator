package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var firstVariable = 0.0
    private var secondVarriable = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }




    private fun init() {
        Button0.setOnClickListener(this)
        Button1.setOnClickListener(this)
        Button2.setOnClickListener(this)
        Button3.setOnClickListener(this)
        Button4.setOnClickListener(this)
        Button5.setOnClickListener(this)
        Button6.setOnClickListener(this)
        Button7.setOnClickListener(this)
        Button8.setOnClickListener(this)
        Button9.setOnClickListener(this)
        Button0.setOnClickListener(this)

        deleteButton.setOnLongClickListener {
            resultTextView.text = ""
            true
        }
        dotButton.setOnClickListener{
            if (resultTextView.text.toString().isNotEmpty() && "." !in resultTextView.text.toString()){
                resultTextView.text = resultTextView.text.toString() + "."
            }

        }
    }

    fun divide(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            operation = ":"
            firstVariable = value.toDouble()
            resultTextView.text = ""
        }
    }
    fun multiply(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty()){
            operation = "x"
            firstVariable = value.toDouble()
            resultTextView.text = ""
        }
    }


    fun minus(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty()){
            operation = "-"
            firstVariable = value.toDouble()
            resultTextView.text = ""
        }
    }
    fun plus(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()){
            operation = "+"
            firstVariable = value.toDouble()
            resultTextView.text = ""
        }

    }



    fun delete(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty())
            resultTextView.text = value.substring(0, value.length - 1)
    }


    fun equal(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty() && operation != "") {
            secondVarriable = value.toDouble()
            var result = 0.0
            if (operation == ":" && secondVarriable == 0.0) {
                resultTextView.text = ""
                Toast.makeText(this, "Zero division error", Toast.LENGTH_SHORT).show()
            } else if (operation == ":" && secondVarriable != 0.0) {
                result = firstVariable / secondVarriable
            } else if (operation == "x") {
                result = firstVariable * secondVarriable
            } else if (operation == "-") {
                result = firstVariable - secondVarriable
            } else if (operation == "+"){
                result = firstVariable + secondVarriable
            }else if(secondVarriable == 0.0){
                equalButton.isClickable = false
            }
                resultTextView.text = result.toString()

        }

    }

    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text = "${resultTextView.text}${button.text}"
    }
}